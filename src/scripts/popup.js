import ext from "./utils/ext";
import storage from "./utils/storage";

let extension_url = 'http://portal.discountr.info/web-extension';

var app = document.getElementById("app");

const main_handler = (data) => {
  let product_id = data.product_id
  let merchant_id = data.merchant_id
  fetch(`${extension_url}/?product_id=${product_id}&merchant_id=${merchant_id}`).then((response) => {
    response.text().then((partial_content) => {
      document.writeln(partial_content)
    });
  })
}

ext.tabs.query({
  active: true,
  currentWindow: true
}, function (tabs) {
  var activeTab = tabs[0];
  chrome.tabs.sendMessage(activeTab.id, {
    action: 'product-info'
  }, main_handler);
});

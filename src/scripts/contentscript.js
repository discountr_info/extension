import ext from "./utils/ext";

var extractTags = () => {
  var url = document.location.href;
  if(!url || !url.match(/^http/)) return;

  var data = {
    title: "",
    description: "",
    url: document.location.href
  }

  var ogTitle = document.querySelector("meta[property='og:title']");
  if(ogTitle) {
    data.title = ogTitle.getAttribute("content")
  } else {
    data.title = document.title
  }

  var descriptionTag = document.querySelector("meta[property='og:description']") || document.querySelector("meta[name='description']")
  if(descriptionTag) {
    data.description = descriptionTag.getAttribute("content")
  }

  return data;
}
const get_product = () => {
  let data_raw = document.body.innerHTML.match('window.__PRODUCT_DETAIL_APP_INITIAL_STATE__ = (\{.*?\});')
  let data_json = JSON.parse(data_raw[1])
  return {product_id: data_json.product.id, merchant_id: data_json.product.merchant.id}
}


function onRequest(request, sender, sendResponse) {
  if (request.action === 'process-page') {
    sendResponse(extractTags())
  }
  if (request.action === 'product-info') {
    sendResponse(get_product())
  }
}

ext.runtime.onMessage.addListener(onRequest);
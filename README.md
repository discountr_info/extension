<div align="center">
  <h1>
    Discountr Web Extension
  </h1>

  <p>
    <strong>Your smart shopping buddy</strong>
  </p>
</div>

View price history, set price alerts and compare prices for millions of goods across Turkish e-commerce stores.

While shopping online, have you ever wanted to see the product's actual price yesterday? How about last week? Or on the 14th of April last year?

Have you ever wanted to set an alert so you get a notification when the price of the product drops below a certain value?

You can do all those things and more with discountr!


<div align="center">
  <a href="https://discountr.info">
    <img src="./resources/chrome-promo/extension.png" alt="Discountr Web Extension">
  </a>
</div>

## Installation
1. Clone the repository `git clone https://gitlab.com/discountr_info/extension.git`
2. Run `npm install`
3. Run `npm run build`


## Developing
The following tasks can be used when you want to start developing the extension and want to enable live reload - 

- `npm run chrome-watch`
- `npm run opera-watch`
- `npm run firefox-watch`


## Packaging
Run `npm run dist` to create a zipped, production-ready extension for each browser. You can then upload that to the appstore.


## Supported Sites
- [x] Trendyol
- [x] Vatan Bilgisayar
- [x] Modanisa
- [x] Boyner
- [x] N11
- [x] Migros
- [x] Defacto
- [x] A101
- [x] Kitapyurdu
- [x] Watsons
- [x] Teknosa
- [x] Carrefoursa
- [x] Koton
- [x] Morhipo
- [x] ePTTAvm


-----------
This project is licensed under the GNU GPLv3 license. 

If you have any questions or comments, please create a new issue. I'd be happy to hear your thoughts.


Discountr, [discountr.info](https://discountr.info)
